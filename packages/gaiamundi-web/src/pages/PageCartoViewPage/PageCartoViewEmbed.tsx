import { PageCartoEmbed } from 'components/PageCarto/View/PageCartoEmbed';
import { DataProvider } from 'hooks/useData';
import { PageCartoProvider } from 'hooks/usePageCarto';
import { useService } from 'hooks/useService';
import { SnapshotProvider } from 'hooks/useSnapshot';
import { useEffect, useRef, useState } from 'react';
import { useQuery } from 'react-query';
import { ContentType } from 'services/abstract';

export const PageCartoViewEmbed: React.FC<{ pageCartoId?: string }> = ({
  pageCartoId,
}) => {
  const id = parseInt(pageCartoId || '');
  const ref = useRef<HTMLDivElement | null>(null);
  const [currentSnapshotId, setCurrentSnapshotId] = useState(0);
  const [selectedGeoCode, setSelectedGeoCode] = useState('');
  const snapshotService = useService(ContentType.SNAPSHOTS);

  const { data: snapshot } = useQuery({
    queryKey: ['snapshot', currentSnapshotId],
    queryFn: async () => {
      return await snapshotService.getSnapshotById(currentSnapshotId);
    },
    keepPreviousData: true,
    enabled: !!currentSnapshotId,
  });

  useEffect(() => {
    // Function to handle click events on links
    const handleLinkClick = (event: Event) => {
      // Find the nearest ancestor <a> tag, if any
      const anchor = (event.target as HTMLElement).closest('a');
      // Check if the clicked element is an anchor tag and its href is an internal link
      if (
        anchor &&
        anchor.tagName === 'A' &&
        anchor.getAttribute('href')?.startsWith('/page-carto')
      ) {
        event.preventDefault(); // Prevent default link behavior
        const newPath = anchor.getAttribute('href') as string;
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const [_slash, _prefix, _id, snapshotId, geoCode] = newPath.split('/');
        snapshotId && setCurrentSnapshotId(parseInt(snapshotId));
        geoCode && setSelectedGeoCode(geoCode);
      }
    };

    if (ref.current) {
      ref.current.addEventListener('click', handleLinkClick);
    }

    return () => {
      if (ref.current) {
        ref.current.removeEventListener('click', handleLinkClick);
      }
    };
  }, []); // Empty dependency array means this effect runs only once

  return (
    <div className="h-full w-full scroll-auto" ref={ref}>
      <PageCartoProvider id={id}>
        <DataProvider pageCartoId={id} geoCodeSelection={selectedGeoCode}>
          <SnapshotProvider snapshot={snapshot}>
            <PageCartoEmbed />
          </SnapshotProvider>
        </DataProvider>
      </PageCartoProvider>
    </div>
  );
};
