import { PageCartoView } from 'components/PageCarto/View/PageCartoView';
import { DataProvider } from 'hooks/useData';
import { PageCartoProvider } from 'hooks/usePageCarto';
import { useService } from 'hooks/useService';
import { SnapshotProvider } from 'hooks/useSnapshot';
import { useQuery } from 'react-query';
import { useParams } from 'react-router-dom';
import { ContentType } from 'services/abstract';

export const PageCartoViewPage: React.FC = () => {
  const params = useParams();
  const id = parseInt(params.id || '');
  const currentSnapshotId = parseInt(params.snapshot || '');
  const selectedGeoCode = params.geocode || '';
  const snapshotService = useService(ContentType.SNAPSHOTS);

  const { data: snapshot } = useQuery({
    queryKey: ['snapshot', currentSnapshotId],
    queryFn: async () => {
      return await snapshotService.getSnapshotById(currentSnapshotId);
    },
    keepPreviousData: true,
    enabled: !!currentSnapshotId,
  });

  return (
    <div className="h-full w-full scroll-auto">
      <PageCartoProvider id={id}>
        <DataProvider pageCartoId={id} geoCodeSelection={selectedGeoCode}>
          <SnapshotProvider snapshot={snapshot}>
            <PageCartoView />
          </SnapshotProvider>
        </DataProvider>
      </PageCartoProvider>
    </div>
  );
};
