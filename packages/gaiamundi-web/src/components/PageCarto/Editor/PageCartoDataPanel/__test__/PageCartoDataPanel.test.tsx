import { fireEvent, render, waitFor } from '@testing-library/react';
import { AuthProvider } from 'hooks/useAuth';
import { ModalProvider } from 'hooks/useModal';
import { QueryClient, QueryClientProvider } from 'react-query';
import { MemoryRouter as Router } from 'react-router-dom';
import { mockDataFragments, mockPageCartoData } from 'utils/mocks/data';
import { PageCartoDataPanel } from '../PageCartoDataPanel';

jest.mock('hooks/usePageCarto', () => {
  return {
    usePageCarto() {
      return {
        data: {
          data: mockPageCartoData,
        },
        columns: mockDataFragments[0].columns,
      };
    },
  };
});

describe('PageCartoPanelData', () => {
  it('should display the data table if data is present', async () => {
    const { getByText, getAllByRole } = render(<PageCartoDataPanel />);

    await waitFor(() => {
      expect(getAllByRole('columnheader')).toHaveLength(5);
      const columns = mockDataFragments[0].columns;
      columns
        .filter((column) => !column.isGeoCode)
        .forEach((column) => {
          const row = getByText(column.name).parentNode;
          expect(row).toHaveTextContent(column.sample.toString());
          expect(row).toHaveTextContent(column.source);
          expect(row).toHaveTextContent(column.validity);
        });
    });
  });

  it('should display useModal menu on click', async () => {
    const queryClient = new QueryClient();
    const mockIntersectionObserver = jest.fn();

    mockIntersectionObserver.mockReturnValue({
      observe: () => null,
      unobserve: () => null,
      disconnect: () => null,
    });

    window.IntersectionObserver = mockIntersectionObserver;
    const { getByTestId, getAllByRole } = render(
      <QueryClientProvider client={queryClient}>
        <Router>
          <AuthProvider>
            <ModalProvider>
              <PageCartoDataPanel />
            </ModalProvider>
          </AuthProvider>
        </Router>
      </QueryClientProvider>
    );

    await waitFor(() => {
      expect(getByTestId('import-dataset')).toBeInTheDocument();
    });

    fireEvent.click(getByTestId('import-dataset'));

    await waitFor(() => {
      const gridModal = getAllByRole('grid')[1];
      expect(gridModal).toBeInTheDocument();
      fireEvent.click(getByTestId('hideModal-button'));
      expect(gridModal).not.toBeInTheDocument();
    });
  });
});
