import { usePageCarto } from 'hooks/usePageCarto';
import { FC } from 'react';
import { PageCartoMap } from '../Editor/PageCartoMap';

export const PageCartoEmbed: FC = () => {
  const { data } = usePageCarto();

  return (
    <div className="pagecarto-container h-full">
      <div className="pagecarto-map-container relative w-[50%] float-right ml-5 z-[999] overflow-hidden">
        <PageCartoMap />
      </div>
      <div
        id="pagecarto-content-container"
        className="pagecarto-content-container border-1 border-blue-600 overflow-y-auto inline"
        dangerouslySetInnerHTML={{ __html: data?.data.html || '' }}
      ></div>

      <div style={{ clear: 'both' }}></div>
      <div className="col-span-6 row-span-2 p-4 relative"></div>
      <div className="col-span-6 row-span-1 my-1"></div>
    </div>
  );
};
