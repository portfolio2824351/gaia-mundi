import { usePageCarto } from 'hooks/usePageCarto';
import { useService } from 'hooks/useService';
import { useState } from 'react';
import { useMutation } from 'react-query';
import { ContentEditable } from '../ContentEditable/ContentEditable';
import { ContentType } from 'services/abstract';

export const TitlePageCartoEdit = () => {
  const { data: pageCarto } = usePageCarto();
  const [pageTitle, setPageTitle] = useState(pageCarto?.data.name || '');
  const pageId = pageCarto?.data.id || 0;
  const pageCartoService = useService(ContentType.PAGE_CARTOS);

  const { mutateAsync, isLoading } = useMutation({
    mutationFn: async () => {
      return await pageCartoService.updatePageCarto(pageId, {
        name: pageTitle,
      });
    },
  });

  const handleChange = (event: React.FormEvent<HTMLHeadingElement>) => {
    const newTitle = event.currentTarget.innerText;
    setPageTitle(newTitle);
  };

  const handleBlur = async () => {
    await mutateAsync();
  };

  return (
    <ContentEditable
      value={pageTitle}
      isLoading={isLoading}
      onInput={handleChange}
      onBlur={handleBlur}
      className="w-full flex-grow text-slate-950 text-xl px-2 focus:outline-none"
    />
  );
};
