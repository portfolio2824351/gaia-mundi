import { ComponentProps } from 'react';

export type LabelProps = ComponentProps<'label'>;

export const Header: React.FC<LabelProps> = ({ children }) => {
  return <h1 className="my-6 text-3xl font-extrabold">{children}</h1>;
};
