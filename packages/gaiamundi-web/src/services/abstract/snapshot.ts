import { ApiDocument } from 'interfaces/api';
import { SnapshotStub, Snapshot } from 'interfaces/snapshot';

export abstract class SnapshotService {
  abstract createSnapshot(
    data: SnapshotStub
  ): Promise<ApiDocument<SnapshotStub>>;
  abstract getSnapshotById(id: number): Promise<ApiDocument<Snapshot>>;
}
