import { getDataTypeMap, guessDomainKey } from '../chart'; // Update with the correct module path and name
import { RawDatum } from 'interfaces/chart';

describe('chart', () => {
  // Test case 1: Test with an empty object
  it('returns an empty object for an empty input', () => {
    const rawDatum = {};
    const result = getDataTypeMap(rawDatum);
    expect(result).toEqual({});
  });

  // Test case 2: Test with an object containing different data types
  it('returns the correct data type map', () => {
    const rawDatum = { x: 1, y: '2', z: true };
    const result = getDataTypeMap(rawDatum);
    const expected = { x: 'number', y: 'string', z: 'boolean' };
    expect(result).toEqual(expected);
  });

  // Test case 3: Test with an object containing nested objects
  it('handles nested objects', () => {
    const rawDatum = { x: 1, y: '2', z: { a: false, b: 3 } };
    const result = getDataTypeMap(rawDatum);
    const expected = { x: 'number', y: 'string', z: 'object' };
    expect(result).toEqual(expected);
  });

  it('returns "id" as the fallback when data is empty', () => {
    const data: RawDatum[] = [];
    const type = 'number';
    const fallbackIndex = 0;

    const result = guessDomainKey(data, type, fallbackIndex);

    expect(result).toEqual('id');
  });

  it('returns the first key with the specified type', () => {
    const data: RawDatum[] = [
      { x: 1, y: '2' },
      { x: 3, y: '4' },
    ];
    const type = 'number';
    const fallbackIndex = 0;

    const result = guessDomainKey(data, type, fallbackIndex);

    expect(result).toEqual('x');
  });

  it('returns the fallback key when no key matches the specified type', () => {
    const data: RawDatum[] = [{ x: '1', y: 2 }];
    const type = 'number';
    const fallbackIndex = 0;

    const result = guessDomainKey(data, type, fallbackIndex);

    expect(result).toEqual('y');
  });
});
