import { ApiData } from './api';
import { GeoProjectionType } from './geojson';
import { PageCarto } from './page-carto';

export interface SnapshotBubbleConfig {
  indicatorId: number;
  colors: string[];
  opacity: number;
  minRadius: number;
  maxRadius: number;
}

export interface SnapshotConfigs {
  indicatorId: number;
  projection: GeoProjectionType;
  colors: string[];
  bubble: SnapshotBubbleConfig;
}

export interface SnapshotBase extends SnapshotConfigs {
  geoCode: string;
}

export interface SnapshotStub extends SnapshotBase {
  page_carto?: number;
}

export interface Snapshot extends SnapshotBase {
  page_carto: ApiData<PageCarto>;
}
