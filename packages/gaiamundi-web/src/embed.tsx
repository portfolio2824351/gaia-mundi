import React from 'react';
import ReactDOM from 'react-dom/client';
import EmbedApp from './EmbedApp';

import './embed.css';

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
window.renderPageCarto = (containerId, props) => {
  const root = ReactDOM.createRoot(
    document.getElementById(containerId) as HTMLElement
  );
  root.render(
    <React.StrictMode>
      <EmbedApp {...props} />
    </React.StrictMode>
  );
};
