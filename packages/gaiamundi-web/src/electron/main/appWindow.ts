import { app, BrowserWindow, shell, protocol } from 'electron';
import path from 'path';
import { inDev } from 'utils/helpers';

// Electron Forge automatically creates these entry points
declare const APP_WINDOW_WEBPACK_ENTRY: string;
declare const APP_WINDOW_PRELOAD_WEBPACK_ENTRY: string;

let appWindow: BrowserWindow;

/**
 * Create Application Window
 * @returns {BrowserWindow} Application Window Instance
 */
export function createAppWindow(): BrowserWindow {
  protocol.registerFileProtocol('local', (request, callback) => {
    // Decode URL to prevent issues with spaces or special characters
    const decodedUrl = decodeURIComponent(request.url);

    // Remove the custom protocol part ('local://') and additional slashes
    const filePath = decodedUrl.replace(/^local:\/\/\/?/, '');

    // If the file exists, proceed to serve it
    callback({ path: filePath });
  });

  // Create new window instance
  appWindow = new BrowserWindow({
    width: 800,
    height: 600,
    backgroundColor: '#202020',
    show: false,
    autoHideMenuBar: true,
    icon: path.resolve('assets/images/appIcon.ico'),
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
      nodeIntegrationInWorker: false,
      nodeIntegrationInSubFrames: false,
      preload: APP_WINDOW_PRELOAD_WEBPACK_ENTRY,
      sandbox: false,
      webSecurity: true,
    },
  });

  // Load the index.html of the app window.
  appWindow.loadURL(APP_WINDOW_WEBPACK_ENTRY);

  // Show window when its ready to
  appWindow.on('ready-to-show', () => appWindow.show());

  // Close all windows when main window is closed
  appWindow.on('close', () => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    appWindow = null;
    app.quit();
  });

  if (inDev()) {
    appWindow.webContents.openDevTools();
  }

  appWindow.webContents.on('will-navigate', (e, url) => {
    const appUrl = new URL(appWindow.webContents.getURL());
    const nextUrl = new URL(url);
    e.preventDefault();
    if (appUrl.origin !== nextUrl.origin) {
      shell.openExternal(url);
    } else if (nextUrl.toString() !== appUrl.toString()) {
      appUrl.hash = nextUrl.pathname;
      appWindow.webContents.loadURL(appUrl.toString());
    }
  });

  return appWindow;
}
