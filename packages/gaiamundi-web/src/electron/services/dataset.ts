import fs from 'fs';
import { ApiData, ApiDocument } from 'interfaces/api';
import { RawDatum } from 'interfaces/chart';
import { Column } from 'interfaces/column';
import { DataFragment, DataFragmentStub } from 'interfaces/data-fragment';
import { Dataset, DatasetStub } from 'interfaces/dataset';
import { UploadedFile } from 'interfaces/file';
import Papa from 'papaparse';
import path from 'path';
import { DatasetService } from 'services/abstract/dataset';
import { ElectronDb } from './electron-db';
import { LOCAL_USER } from './user.ts';
import { GEO_CODE } from 'utils/constants';
import { ContentType } from 'services/abstract';

export type CsvCellValue = string | number | boolean | null;

export type GeoIndexedData = Record<string, Record<string, CsvCellValue>>;

// Class RemoteDatasetService that extends DatasetService
export class LocalDatasetService extends DatasetService {
  private db: ElectronDb;
  private csvDir: string;

  constructor(db: ElectronDb) {
    super();
    this.db = db;

    this.db.createTable(ContentType.DATASET);
    this.db.createTable(ContentType.DATA_FRAGMENT);

    this.csvDir = path.join(this.db.location, 'csv');
    if (!fs.existsSync(this.csvDir)) {
      fs.mkdirSync(this.csvDir, { recursive: true });
    }
  }

  async addDataToPageCarto(
    pageCartoId: number,
    name: string,
    data: DatasetStub,
    columns: Column[]
  ): Promise<ApiDocument<DataFragmentStub>> {
    const dataset = await this.db.insertTableContent(ContentType.DATASET, data);
    return await this.db.insertTableContent(ContentType.DATA_FRAGMENT, {
      name: name,
      columns: columns,
      dataset: dataset.data.id,
      page_carto: pageCartoId,
    });
  }

  async uploadCsv(_file: File): Promise<void> {
    throw new Error('Not implemented!');
    //   await this.strapiService.uploadFile(file, 'api::dataset.dataset');
  }

  private pickCsvColumns(
    csvRows: Array<Record<string, CsvCellValue>>,
    columns: string[]
  ): Array<Record<string, CsvCellValue>> {
    return csvRows.map((row) => {
      return columns.reduce((result, column) => {
        result[column] = row[column];
        return result;
      }, {} as Record<string, CsvCellValue>);
    });
  }

  private parseCsvFile(filePath: string) {
    if (fs.existsSync(filePath)) {
      const fileContent = fs.readFileSync(filePath, 'utf-8');
      return Papa.parse<Record<string, CsvCellValue>>(fileContent, {
        header: true,
        dynamicTyping: true,
        transformHeader: (headerName: string) => {
          // Remove metadata (source & validity) & remove extra spaces
          return headerName.replace(/\[[^\]]*\]/i, '').trim();
        },
      });
    }
    return Promise.reject(new Error(`Unable to read file : ${filePath}`));
  }

  private indexDataByGeoCode(
    data: Array<Record<string, CsvCellValue>>,
    geoCodeColumn: string
  ) {
    return data.reduce((acc, curr) => {
      const geoCode = curr[geoCodeColumn] as string;
      if (geoCode) {
        acc[geoCode] = {
          ...curr,
        };
        delete acc[geoCode][geoCodeColumn];
      }
      return acc;
    }, {} as GeoIndexedData);
  }

  private async parseDataByFragment(fragment: ApiData<DataFragment>) {
    const geoCodeColumn = fragment.columns.find(({ isGeoCode }) => isGeoCode);
    if (!geoCodeColumn) {
      throw new Error(
        `Unable to find the geocode for fragment #${fragment.id}`
      );
    }
    const { data } = await this.parseCsvFile(
      path.join(this.csvDir, `${fragment.dataset.csv.id}.csv`)
    );
    const columns: string[] = fragment.columns.map(({ name }) => name);
    const fragmentData = this.pickCsvColumns(data, columns);
    return this.indexDataByGeoCode(fragmentData, geoCodeColumn.name);
  }

  private mergeDataByGeocode(dataCollection: GeoIndexedData[]) {
    return dataCollection.reduce((dataDict, geoIndexedData) => {
      return Object.entries(geoIndexedData).reduce((acc, [geoCode, row]) => {
        acc[geoCode] = {
          ...(geoCode in acc ? acc[geoCode] : {}),
          ...row,
          [GEO_CODE]: geoCode,
        };
        return acc;
      }, dataDict);
    }, {});
  }

  async getPageCartoData(_id: number): Promise<RawDatum[]> {
    const fragments = await this.getDataFragmentsByPageCartoId(_id);
    const geoIndexedDataCollection: GeoIndexedData[] = await Promise.all(
      fragments.map((fragment) => {
        return this.parseDataByFragment(fragment);
      })
    );
    const geoIndexedData = this.mergeDataByGeocode(geoIndexedDataCollection);

    return Object.values(geoIndexedData);
  }

  async getDatasetById(id: number): Promise<ApiData<Dataset>> {
    const { data: dataset } = await this.db.getById<DatasetStub>(
      ContentType.DATASET,
      id
    );
    const { data: csv } = await this.db.getById<UploadedFile>(
      ContentType.FILES,
      dataset.csv
    );
    return {
      ...dataset,
      csv: csv,
      owner: LOCAL_USER,
    };
  }

  async getDataFragmentsByPageCartoId(
    pageCartoId: number
  ): Promise<ApiData<DataFragment>[]> {
    const fragments = await this.db.search<DataFragmentStub>(
      ContentType.DATA_FRAGMENT,
      'page_carto',
      pageCartoId
    );
    const result = fragments.data.map(async (fragment) => {
      return {
        ...fragment,
        dataset: await this.getDatasetById(fragment.dataset),
      };
    });
    return await Promise.all(result);
  }
}
