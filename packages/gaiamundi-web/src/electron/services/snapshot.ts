import { ApiDocument } from 'interfaces/api';
import { Snapshot, SnapshotStub } from 'interfaces/snapshot';
import { SnapshotService } from 'services/abstract/snapshot';
import { ElectronDb } from './electron-db';
import { ContentType } from 'services/abstract';

export class LocalSnapshotService extends SnapshotService {
  private db: ElectronDb;

  constructor(db: ElectronDb) {
    super();
    this.db = db;

    this.db.createTable(ContentType.SNAPSHOTS);
  }

  async createSnapshot(data: SnapshotStub): Promise<ApiDocument<SnapshotStub>> {
    return await this.db.insertTableContent<SnapshotStub>(
      ContentType.SNAPSHOTS,
      data
    );
  }

  async getSnapshotById(id: number): Promise<ApiDocument<Snapshot>> {
    return await this.db.getById<Snapshot>(ContentType.SNAPSHOTS, id);
  }
}
