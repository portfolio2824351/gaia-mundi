import { ApiCollection, ApiData, ApiDocument } from 'interfaces/api';
import { Chart } from 'interfaces/chart';
import { ChartService } from 'services/abstract/chart';
import { ElectronDb } from './electron-db';
import { ContentType } from 'services/abstract';

export class LocalChartService extends ChartService {
  private db: ElectronDb;

  constructor(db: ElectronDb) {
    super();
    this.db = db;

    this.db.createTable(ContentType.CHARTS);
  }

  async createChart(data: Chart): Promise<ApiDocument<Chart>> {
    return await this.db.insertTableContent(ContentType.CHARTS, data);
  }

  async getChartById(id: number): Promise<ApiDocument<Chart>> {
    return await this.db.getById(ContentType.CHARTS, id);
  }

  async getChartsByCartoPage(
    pageCartoId: number
  ): Promise<ApiCollection<Chart>> {
    return this.db.search<Chart>(ContentType.CHARTS, 'page_carto', pageCartoId);
  }

  async updateChart(
    id: number,
    data: Partial<Chart>
  ): Promise<ApiDocument<Chart>> {
    await this.db.updateRow(ContentType.CHARTS, { id: id }, data);

    return await this.getChartById(id);
  }

  async deleteChart(id: number) {
    return await this.db.deleteRow<ApiData<Chart>>(ContentType.CHARTS, {
      id: id,
    });
  }
}
