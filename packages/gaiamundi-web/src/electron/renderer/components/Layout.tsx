import { Outlet, useLocation } from 'react-router-dom';

import { ModalProvider } from 'hooks/useModal';
import Menu from './Menu';
import classNames from 'classnames';

const isFullScreenRegex = /\/page-carto\/[0-9]+(\/([0-9]+|edit)(.+)?)?/gm;

export const Layout: React.FC = () => {
  const location = useLocation();
  const isFullScreen = isFullScreenRegex.test(location.pathname);
  return (
    <div
      className={classNames(
        'min-h-screen w-full',
        isFullScreen && 'overflow-hidden'
      )}
    >
      <ModalProvider>
        <div className={'w-screen h-screen'}>
          <Menu isFluid={true} />
          <div className={'relative w-full h-full p-6 box-border'}>
            <Outlet />
          </div>
        </div>
      </ModalProvider>
    </div>
  );
};
