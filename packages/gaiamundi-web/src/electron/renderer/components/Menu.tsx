import { PlusIcon } from '@heroicons/react/24/solid';
import classNames from 'classnames';
import { Link, useLocation, useNavigate } from 'react-router-dom';

import { Button } from 'components/Button/Button';
import { LogoLink } from 'components/Layout/LogoLink';

const NavbarMenuItem: React.FC<{
  href: string;
  title: string;
  isInverted: boolean;
}> = ({ href, title, isInverted = false }) => {
  const location = useLocation();
  const isCurrentPage = location.pathname === href;
  const lightClasses = {
    active: `text-gray-900 bg-gray-200 focus:text-gray-600`,
    inactive: `text-gray-600 hover:text-gray-900 hover:bg-gray-100 focus:outline-none`,
  };
  const darkClasses = {
    active: `text-white bg-royal-gray-800 focus:text-white`,
    inactive: `text-gray-100 hover:text-white hover:bg-royal-gray-800 focus:outline-none`,
  };
  const classes = isInverted ? darkClasses : lightClasses;
  return (
    <Link
      to={href}
      className={`mr-4 px-3 py-2 rounded text-sm font-medium focus:outline-none ${
        isCurrentPage ? classes.active : classes.inactive
      }`}
    >
      {title}
    </Link>
  );
};

const loggedInMenuItems = [{ title: 'Tableau de bord', href: '/' }];

const NavbarMenu: React.FC<{ isInverted: boolean }> = ({ isInverted }) => {
  return (
    <div data-testid="navigations" className="flex items-baseline ml-10">
      {loggedInMenuItems.map(({ title, href }) => {
        return (
          <NavbarMenuItem
            key={title}
            href={href}
            title={title}
            isInverted={isInverted}
          />
        );
      })}
    </div>
  );
};

const SideNavigation: React.FC = () => {
  const navigate = useNavigate();
  const shouldShow = location.pathname !== '/page-carto/create';
  return (
    <div className="flex flex-row items-center hidden md:flex">
      {shouldShow && (
        <div className="flex flex-row">
          <Button
            outline={false}
            size="md"
            className="inline-flex mx-1"
            color={'lime'}
            icon={PlusIcon}
            data-testid="newPageCarto-button"
            onClick={() => navigate('/page-carto/create')}
          >
            Nouvelle PageCarto
          </Button>
        </div>
      )}
    </div>
  );
};

type MenuProps = {
  isFluid?: boolean;
  isInverted?: boolean;
};

const Menu: React.FC<MenuProps> = ({ isFluid = false, isInverted = false }) => {
  return (
    <nav
      className={classNames(
        isFluid ? 'w-full' : '',
        isInverted ? 'bg-royal-blue-900 text-gray-700 body-font' : 'bg-white',
        'shadow-md z-10'
      )}
    >
      <div
        className={classNames(
          'sm:px-6 lg:px-8',
          isFluid ? 'w-full' : 'mx-auto max-w-7xl'
        )}
      >
        <div className="flex items-center justify-between h-16 px-4 sm:px-0">
          <div className="flex items-center">
            <LogoLink isInverted={isInverted} />
            <NavbarMenu isInverted={isInverted} />
          </div>
          <SideNavigation />
        </div>
      </div>
    </nav>
  );
};

export default Menu;
