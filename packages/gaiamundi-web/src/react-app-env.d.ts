/// <reference types="react-scripts" />
declare module 'excerpt-html' {
  export default function (html: string): string;
}

declare module 'electron-db' {
  interface SuccessCallback {
    (success: boolean, message: string): void;
  }

  interface RowData {
    [key: string]: any;
  }

  interface SearchCallback<T> {
    (success: boolean, data: T[]): void;
  }

  interface FieldCallback<T> {
    (success: boolean, data: Partial<T>[]): void;
  }

  interface CountCallback {
    (success: boolean, count: number): void;
  }

  function createTable(
    tableName: string,
    location: string,
    callback: SuccessCallback
  ): void;

  function insertTableContent<T>(
    tableName: string,
    location: string,
    data: ApiData<T>,
    callback: SuccessCallback
  ): void;

  function valid(tableName: string): boolean;

  function getAll<T>(
    tableName: string,
    location: string,
    callback: (success: boolean, data: ApiData<T>[]) => void
  ): void;

  function getRows<T>(
    tableName: string,
    location: string,
    conditions: Partial<T>,
    callback: (success: boolean, result: ApiData<T>[]) => void
  ): void;

  function updateRow<T>(
    tableName: string,
    location: string,
    where: Partial<T>,
    set: Partial<T>,
    callback: SuccessCallback
  ): void;

  function search<T>(
    tableName: string,
    location: string,
    key: string,
    term: string | number | boolean,
    callback: SearchCallback<ApiData<T>>
  ): void;

  function deleteRow<T>(
    tableName: string,
    location: string,
    conditions: Partial<T>,
    callback: SuccessCallback
  ): void;

  function getField<T>(
    tableName: string,
    location: string,
    key: keyof T,
    callback: FieldCallback<Pick<T, typeof key>>
  ): void;

  function count(
    tableName: string,
    location: string,
    callback: CountCallback
  ): void;
}
