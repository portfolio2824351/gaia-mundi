import { QueryClient, QueryClientProvider } from 'react-query';
import { PageCartoViewEmbed } from 'pages/PageCartoViewPage/PageCartoViewEmbed';

// Create a client
const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false, // default: true
    },
  },
});

export default function EmbedApp({ pageCartoId }: { pageCartoId?: string }) {
  return (
    <QueryClientProvider client={queryClient}>
      <PageCartoViewEmbed pageCartoId={pageCartoId} />
    </QueryClientProvider>
  );
}
