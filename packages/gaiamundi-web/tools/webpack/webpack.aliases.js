const { createWebpackAliases } = require('./webpack.helpers');

// Export aliases
module.exports = createWebpackAliases({
  pages: 'src/pages',
  components: 'src/components',
  interfaces: 'src/interfaces',
  hooks: 'src/hooks',
  utils: 'src/utils',
  services: 'src/services',
  config: 'src/config',
  // ElectronJS related
  '@assets': 'assets',
  '@components': 'src/electron/renderer/components',
  '@common': 'src/common',
  '@main': 'src/main',
  '@renderer': 'src/electron/renderer',
  '@src': 'src',
});
